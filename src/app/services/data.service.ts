import { Injectable, OnInit } from '@angular/core'

import { HttpClient } from '@angular/common/http'
import { LoginService } from '../login/login.service'
import { Router } from '@angular/router'
import { User } from '../user.model'

@Injectable()
export class DataService implements OnInit {
  token: string = ''
  constructor(private httpClient: HttpClient, private route: Router) {}
  ngOnInit(): void {
    this.token = localStorage.getItem('Token') as string
  }

  getEmpleados() {
    this.token = localStorage.getItem('Token') as string
    return this.httpClient.get(`https://registro-de-empleados-e6738-default-rtdb.firebaseio.com/empleados.json?auth=${this.token}`)
  }

  saveEmpleados(empleados: User[]): void {
    console.log(empleados)
    this.token = localStorage.getItem('Token') as string
    this.httpClient.put(`https://registro-de-empleados-e6738-default-rtdb.firebaseio.com/empleados.json?auth=${this.token}`, empleados).subscribe((resp) => {
      console.log('datos guardados exitosamente')
      this.route.navigate(['/main'])
    })
  }

  updateEmpleados(indice: number, empleado: User): void {
    this.token = localStorage.getItem('Token') as string
    let url = `https://registro-de-empleados-e6738-default-rtdb.firebaseio.com/empleados/${indice}.json?auth=${this.token}`
    this.httpClient.put(url, empleado).subscribe((data) => {
      console.log('Usuario Actualizado correctamente', data)
      this.route.navigate(['/main'])
    })
  }

  deleteEmpleado(indice: string) {
    this.token = localStorage.getItem('Token') as string
    let url = `https://registro-de-empleados-e6738-default-rtdb.firebaseio.com/empleados/${indice}.json?auth=${this.token}`
    this.httpClient.delete(url).subscribe((data) => {
      console.log('Usuario eliminado correctamente')
    })
  }
}
