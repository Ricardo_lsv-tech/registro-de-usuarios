import { DataService } from './data.service'
import { Injectable } from '@angular/core'
import { User } from '../user.model'

@Injectable()
export class AddUserServiceService {
  users: User[] = []
  constructor(private dataService: DataService) {}

  setUsers(users: User[]) {
    this.users = users
  }
  getUsers() {
    return this.dataService.getEmpleados()
  }

  addUserService(user: User) {
    if (this.users === null) {
      this.users = []
    }
    this.users.push(user)
    this.dataService.saveEmpleados(this.users)
  }

  updateEmpleadoService(indice: number, empleado: User) {
    let newEmpleado = this.users[indice]
    newEmpleado!.nombre = empleado.nombre
    newEmpleado!.apellido = empleado.apellido
    newEmpleado!.cargo = empleado.cargo
    newEmpleado!.salario = empleado.salario
    this.dataService.updateEmpleados(indice, empleado)
  }

  deleteEmpleado(indice: string) {
    this.users.splice(Number(indice), 1)
    this.dataService.deleteEmpleado(indice)
    this.updateNewUsers()
  }

  updateNewUsers() {
    if (this.users !== null) {
      this.dataService.saveEmpleados(this.users)
    }
  }
}
