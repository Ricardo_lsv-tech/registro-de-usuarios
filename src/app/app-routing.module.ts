import { RouterModule, Routes } from '@angular/router'
import { canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard'

import { ContactoComponentComponent } from './components/contacto-component/contacto-component.component'
import { HomeComponentComponent } from './components/home-component/home-component.component'
import { LoginComponent } from './login/login.component'
import { NgModule } from '@angular/core'
import { ProyectoComponentComponent } from './components/proyecto-component/proyecto-component.component'
import { QuienesComponentComponent } from './components/quienes-component/quienes-component.component'
import { UpdateEmpleadosComponent } from './components/update-empleados/update-empleados.component'

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'main' },
  { path: 'main', component: HomeComponentComponent, ...canActivate(() => redirectUnauthorizedTo(['/login'])) },
  { path: 'proyectos', component: ProyectoComponentComponent, ...canActivate(() => redirectUnauthorizedTo(['/login'])) },
  { path: 'quienes', component: QuienesComponentComponent, ...canActivate(() => redirectUnauthorizedTo(['/login'])) },
  { path: 'contacto', component: ContactoComponentComponent, ...canActivate(() => redirectUnauthorizedTo(['/login'])) },
  { path: 'actualizar/:indice', component: UpdateEmpleadosComponent, ...canActivate(() => redirectUnauthorizedTo(['/login'])) },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
