import { Auth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from '@angular/fire/auth'

import { DataService } from '../services/data.service'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'

@Injectable()
export class LoginService {
  constructor(private auth: Auth, private route: Router) {}

  loginRegister({ email, password }: { email: string; password: string }) {
    return createUserWithEmailAndPassword(this.auth, email, password)
  }

  async loginSignIn({ email, password }: { email: string; password: string }) {
    try {
      const res = await signInWithEmailAndPassword(this.auth, email, password)
      const token = await res.user.getIdToken()
      localStorage['Token'] = token
      this.route.navigate(['/main'])
    } catch (error) {
      return console.log(error)
    }
  }

  logout() {
    return signOut(this.auth)
  }
}
