import { Component, Injectable } from '@angular/core'

import { DataService } from '../services/data.service'
import { LoginService } from './login.service'
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  token: string
  constructor(private loginService: LoginService) {}
  login(form: NgForm) {
    const email = form.value.email
    const password = form.value.password
    this.loginService.loginSignIn({ email, password })
  }
}
