import { getAuth, provideAuth } from '@angular/fire/auth'
import { initializeApp, provideFirebaseApp } from '@angular/fire/app'

import { AddUserServiceService } from './services/add-user-service.service'
import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'
import { BrowserModule } from '@angular/platform-browser'
import { ContactoComponentComponent } from './components/contacto-component/contacto-component.component'
import { DataService } from './services/data.service'
import { FormComponent } from './components/form/form.component'
import { FormsModule } from '@angular/forms'
import { HomeComponentComponent } from './components/home-component/home-component.component'
import { HttpClientModule } from '@angular/common/http'
import { LoginComponent } from './login/login.component'
import { LoginService } from './login/login.service'
import { NavbarComponent } from './components/navbar/navbar.component'
import { NgModule } from '@angular/core'
import { ProyectoComponentComponent } from './components/proyecto-component/proyecto-component.component'
import { QuienesComponentComponent } from './components/quienes-component/quienes-component.component'
import { UpdateEmpleadosComponent } from './components/update-empleados/update-empleados.component'
import { UserTableComponent } from './components/user-table/user-table.component'
import { environment } from '../environments/environment'

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    UserTableComponent,
    NavbarComponent,
    HomeComponentComponent,
    ProyectoComponentComponent,
    QuienesComponentComponent,
    ContactoComponentComponent,
    UpdateEmpleadosComponent,
    LoginComponent
  ],
  imports: [BrowserModule, FormsModule, AppRoutingModule, HttpClientModule, provideFirebaseApp(() => initializeApp(environment.firebase)), provideAuth(() => getAuth())],
  providers: [LoginService, DataService, AddUserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {}
