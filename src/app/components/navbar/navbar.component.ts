import { Component } from '@angular/core'
import { LoginService } from 'src/app/login/login.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(private loginService: LoginService, private route: Router) {}

  logout() {
    this.loginService
      .logout()
      .then(() => {
        localStorage.removeItem('Token')
        this.route.navigate(['/login'])
      })
      .catch((err) => {
        console.log(err)
      })
  }
}
