import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-proyecto-component',
  templateUrl: './proyecto-component.component.html',
  styleUrls: ['./proyecto-component.component.css']
})
export class ProyectoComponentComponent {
  constructor(private route: Router) {}
  toBack(): void {
    this.route.navigate([''])
  }
}
