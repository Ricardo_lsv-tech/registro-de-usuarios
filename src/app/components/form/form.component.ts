import { AddUserServiceService } from 'src/app/services/add-user-service.service'
import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { User } from '../../user.model'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  constructor(private userDataService: AddUserServiceService, private route: Router) {}
  id: string = ''
  nombre: string = ''
  apellido: string = ''
  cargo: string = ''
  salario: number = 0

  addUser() {
    let userData = new User((this.id = crypto.randomUUID()), this.nombre, this.apellido, this.cargo, Number(this.salario))
    this.userDataService.addUserService(userData)
    this.nombre = ''
    this.apellido = ''
    this.cargo = ''
    this.salario = 0
  }
}
