import { ActivatedRoute, Router } from '@angular/router'
import { Component, OnInit } from '@angular/core'

import { AddUserServiceService } from 'src/app/services/add-user-service.service'
import { User } from 'src/app/user.model'

@Component({
  selector: 'app-update-empleados',
  templateUrl: './update-empleados.component.html',
  styleUrls: ['./update-empleados.component.css']
})
export class UpdateEmpleadosComponent implements OnInit {
  empleados!: User[]
  constructor(private userData: AddUserServiceService, private route: Router, private routeParams: ActivatedRoute) {}

  ngOnInit(): void {
    this.empleados = this.userData.users
    this.indice = this.routeParams.snapshot.params['indice']
    let empleado = this.empleados[this.indice]
    this.nombre = empleado!.nombre
    this.apellido = empleado!.apellido
    this.cargo = empleado!.cargo
    this.salario = empleado!.salario
    this.id = empleado.id
  }

  updateEmpleado() {
    let newEmpleado = new User(this.id, this.nombre, this.apellido, this.cargo, this.salario)
    this.userData.updateEmpleadoService(this.indice, newEmpleado)
  }
  nombre: string = ''
  apellido: string = ''
  cargo: string = ''
  salario: number = 0
  id: string = ''
  indice: number = 0
}
