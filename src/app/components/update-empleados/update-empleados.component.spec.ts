import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEmpleadosComponent } from './update-empleados.component';

describe('UpdateEmpleadosComponent', () => {
  let component: UpdateEmpleadosComponent;
  let fixture: ComponentFixture<UpdateEmpleadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateEmpleadosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
