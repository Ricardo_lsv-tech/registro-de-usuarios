import { Component, Injectable, OnInit } from '@angular/core'

import { AddUserServiceService } from 'src/app/services/add-user-service.service'
import { BehaviorSubject } from 'rxjs'
import { DataService } from '../../services/data.service'
import { Router } from '@angular/router'
import { User } from '../../user.model'

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  usersList: User[] = []
  constructor(private addUserService: AddUserServiceService, private route: Router) {
    this.addUserService.getUsers().subscribe((users) => {
      if (users === null) {
        this.usersList = []
      } else {
        this.usersList = Object.values(users)
        this.addUserService.setUsers(Object.values(users))
      }
    })
  }

  ngOnInit(): void {}

  editEmpleado(indice: number): void {
    this.route.navigate([`/actualizar/${indice}`])
  }

  deleteEmpleado(indice: string): void {
    this.usersList.splice(Number(indice), 1)
    this.addUserService.deleteEmpleado(indice)
  }
}
