import { Component } from '@angular/core'
import { User } from 'src/app/user.model'

@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent {
  title = 'User List'
  user!: User

  addUser(user: User): void {
    this.user = user
  }
}
